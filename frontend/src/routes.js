import React, { Component } from "react";
import "./App.css";
import { Jumbotron } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import CustomersClient from "./components/customer/customers";
import SuppliersClient from "./components/suppliers-client/suppliers";
import Login from "./components/login/login";
import Header from "./components/header/header";
import HomeCard from "./components/home-card/home-card";

class AppRouting extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Router>
        <Header />
        <div className="App">
          <h1
            style={{
              fontSize: "50px",
              marginTop: "50px",
              marginBottom: "30px"
            }}
          >
            Sets Decentralized E-Commerce
          </h1>
          <hr style = {{
            width: "100px",
            borderTop: "5px solid rgba(16, 185, 129, 1)",
            marginBottom: "50px"
          }}></hr>
          {/* STEPS:
          1. Add items for sale by supplier.
          2. Display them in customer section.
          3. Purchase an Item from customer section.
          4. Display purchaseOrder in Supplier section.
          5. Complete order in Supplier section.
          6. Show order completed in Customer section.
        */}

          <Switch>
            <Route path="/customer">
              <CustomersClient />
            </Route>
            <Route path="/supplier">
              <SuppliersClient />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                  <HomeCard
                    bgColor="white"
                    textColor="rgba(75, 85, 99,1)"
                    title="Customer"
                    link = "/customer"
                  />

                  <HomeCard
                    bgColor="rgba(110, 231, 183,1)"
                    textColor="rgba(55, 65, 81,1)"
                    title="Supplier"
                    link = "/login"
                  />
              </div>
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default AppRouting;
