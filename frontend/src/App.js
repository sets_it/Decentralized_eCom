import React from "react"
import './App.css';
import AppRouting from './routes';

function App () {
    return (
      <AppRouting/>
    );

}

export default App;
