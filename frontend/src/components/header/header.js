import React from "react"

export default function Header(){
	return(
			<div style = {{
				width: "100vw",
			}}>
					<img alt = "error displaying header..." src ={process.env.PUBLIC_URL + "/images/sets-header.png"} style = {{objectFit: 'cover', maxWidth: "100%"}}/>
			</div>
	)
};