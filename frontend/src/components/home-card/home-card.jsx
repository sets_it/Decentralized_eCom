import React from "react";
import styles from "./home-card.module.css";
import { Link } from "react-router-dom";

function HomeCard({ bgColor, textColor, title, link }) {
  return (
    <Link to={link} style = {{
			textDecoration: "none",
			color: textColor,
		}}>
      <div className="homeCard" style = {{
				backgroundColor: bgColor
			}}>
        <h3>{title}</h3>
      </div>
    </Link>
  );
}

export default HomeCard;
