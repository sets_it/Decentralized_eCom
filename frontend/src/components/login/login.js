import React from "react";
import { Link } from "react-router-dom";
import {
  Form,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Button,
} from "react-bootstrap";

function Login() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        width: "100vw",
        alignItems: "center",
      }}
    >
      <div
        style={{
          width: "50%",
          display: "flex",
          flexDirection: "column",
          alignItems: "start",
          margin: "15px",
        }}
      >
        <div
          style={{
            fontSize: "20px",
            fontWeight: "100",
            color: "rgba(75, 85, 99, 1)",
            marginBottom: "5px",
          }}
        >
          Name
        </div>
        <input
          type="text"
          value="SETS member"
          readOnly
          style={{
            width: "100%",
            padding: "10px",
            fontSize: "18px",
          }}
        />
      </div>

      <div
        style={{
          width: "50%",
          display: "flex",
          flexDirection: "column",
          alignItems: "start",
          margin: "15px",
        }}
      >
        <div
          style={{
            fontSize: "20px",
            fontWeight: "100",
            color: "rgba(75, 85, 99, 1)",
            marginBottom: "5px",
          }}
        >
          Password
        </div>
        <input
          type="password"
          value="SETSisawesome"
          readOnly
          style={{
            width: "100%",
            padding: "10px",
            fontSize: "18px",
          }}
        />
      </div>
      <Link to="/supplier">
        <button style = {{
          margin: "20px",
          padding: "15px 30px",
          borderRadius: "100px",
          border: "none",
          backgroundColor: "rgba(219, 39, 119,1)",
          color: "white",
          fontWeight: "50px",
        }}>
          SUBMIT
        </button>
      </Link>
    </div>
  );
}

export default Login;
